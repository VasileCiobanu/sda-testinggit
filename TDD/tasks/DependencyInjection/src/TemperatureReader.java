public class TemperatureReader {
    private Thermometer thermometer;

    public TemperatureReader() {
        thermometer = new RandomThermometer();
    } // constructor are acelas nume ca si clasa
    public TemperatureReader(Thermometer thermometer){
        this.thermometer = thermometer;
    }
    public double readTemperature() {
        if(thermometer != null) return thermometer.readTemperature();
        return Double.MIN_VALUE;
    }

}
