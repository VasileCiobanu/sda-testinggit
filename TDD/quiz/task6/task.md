# Mocking 

Mocking is substituting a dummy object that you don't want to use at the moment. Mocking is very useful for creating unit tests - which must be independent of external dependencies. This technique is also used at an early stage of development, when not all classes are available anymore, but are needed to work on other parts of the system. 

Do you always need to implement all methods when creating a Mock of a given class? 

a) True for mock to work, all methods must be implemented. 

b) Partially true to be able to compile a program, mock must implement all of the methods. You can omit methods that are not referenced. 

c) False, in many cases only the return of predefined values can be used, and if we do not specify them, the default values will be returned.